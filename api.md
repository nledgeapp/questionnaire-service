# Questionnaire API

## GET /questionnaire?fields=_id,title,description

### Response:
```
[
	{
		_id: String,
		title: String
		description: String
	}
]
```

## GET /questionnaire/id

### Response:
```
{
	_id: String,
	title: String
	description: String`
	questions: [
		{
			id: String
			question: String
			min_choises: Int
			max_choises: Int
			alternatives: [
				{
					attribute_id: String
					text: String
				}
			]
		}
	]
}
```
