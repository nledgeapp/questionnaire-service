import mongoose from 'mongoose';
let Schema = mongoose.Schema;

export default new Schema({
        question: String,
        alternatives: [Number],
        questionnaire_id: String
    },
    {collection: 'question'}
)
