import mongoose from 'mongoose';
let Schema = mongoose.Schema;

export default new Schema({
        title: String,
        description: String
    },
    {collection: 'questionnaire'}
)
