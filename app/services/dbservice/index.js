import path from 'path';
import mongoose from 'mongoose';
import questionSchema from './schemas/question';
import questionnaireSchema from './schemas/questionnaire';

mongoose.Promise = global.Promise;

let root = path.dirname(require.main.filename),
    config = require(path.normalize(root+'/config')),
    dbModels = {};

export default class DatabaseService {

    constructor() {}

    connectDb() {
        return new Promise((resolve, reject)=> {
            mongoose.connect(config.mongo.uri, config.mongo.options)
            .then((resp)=> {
                this.configureDbModels();
                resolve();
            })
            .catch((err)=> {
                reject(err);
            });
        });
    }

    configureDbModels() {
        dbModels.Question = mongoose.model('Question', questionSchema);
        dbModels.Questionnaire = mongoose.model('Questionnaire', questionnaireSchema);
    }

    getDbModels() {
        return dbModels;
    }
}
