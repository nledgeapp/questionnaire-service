
import express from 'express';
import question from './question';
import questionnaire from './questionnaire';

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
  var router = express.Router();

  router.use('/question', question(app, dbService));
  router.use('/questionnaire', questionnaire(app, dbService));

  return router;
};
