'use strict';
export default class QuestionController {

	constructor(params){
		this.Questionnaire = params.dbService.getDbModels().Questionnaire;
	}
	// POST
	createQuestionnaire({body} = {}) {
		return new Promise((resolve, reject)=> {
			let newQuestionnaire = new this.Questionnaire(body);
			newQuestionnaire.save((err, result)=> {
				if (err) {
					console.log('error: ',err);
					reject(err);
				} else {
					console.log('resolving');
					resolve({result});
				}
			});
		});
	}

	// PUT
	editQuestionnaire({params, body} = {}) {
		return new Promise((resolve, reject)=> {
			this.Questionnaire.update(params, body, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

	// GET
	findAllQuestionnaires() {
		return new Promise((resolve, reject)=> {
			this.Questionnaire.find({}, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

	// GET
	findQuestionnaire({params} = {}) {
		return new Promise((resolve, reject)=> {
			this.Questionnaire.find(params, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

	// DELETE
	deleteQuestionnaire({params} = {}) {
		return new Promise((resolve, reject)=> {
			this.Questionnaire.findByIdAndRemove(params, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve(result);
				}
			});
		});
	}
}
