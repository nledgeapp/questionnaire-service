
import express from 'express';
import QuestionnaireController from './questionnaire.controller';

let path = require('path'),
    root = path.dirname(require.main.filename);

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
    let router = express.Router(),
        questionnaireController = new QuestionnaireController({dbService});

    router.post("/", (req, res)=> {
        let newQuestionnaire = req.body;
        questionnaireController.createQuestionnaire({body:newQuestionnaire})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.put("/:_id", (req, res)=> {
        let params = req.params,
            body = req.body;

        questionnaireController.editQuestionnaire({params, body})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.get("/", (req, res)=> {
        questionnaireController.findAllQuestionnaires()
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.get("/:_id", (req, res)=> {
        let params = req.params;

        questionnaireController.findQuestionnaire({params})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.delete("/:_id", (req, res)=> {
        let params = req.params;

        questionnaireController.deleteQuestionnaire({params})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

  return router;
};
