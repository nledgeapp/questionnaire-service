'use strict';

export default class QuestionController {

	constructor(params) {
        this.Question = params.dbService.getDbModels().Question;
	}

	// POST
	createQuestion({body} = {}) {
		return new Promise((resolve, reject)=> {
			let newQuestion = new this.Question(body);
			newQuestion.save((err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve ({result});
				}
			});
		});
	}

	// PUT
	editQuestion({params, body} = {}) {
		return new Promise((resolve, reject)=> {
			this.Question.update(params, body, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

	// GET
	findAllQuestion({query} = {}) {
		return new Promise((resolve, reject)=> {
			this.Question.find(query, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

	// GET
	findQuestions({params} = {}) {
		return new Promise((resolve, reject)=> {
			this.Question.find(params, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

	// DELETE
	deleteQuestion({params} = {}) {
		return new Promise((resolve, reject)=> {
			this.Question.findByIdAndRemove(params._id, (err, result)=> {
				if (err) {
					reject(err);
				} else {
					resolve({result});
				}
			});
		});
	}

}
