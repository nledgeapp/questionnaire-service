import express from 'express';
import QuestionController from './question.controller';

let path = require('path'),
    root = path.dirname(require.main.filename);

/** Route : `/api/[ROUTE]`*/
export default (app, dbService) => {
    let router = express.Router(),
        questionController = new QuestionController({dbService});

    router.post("/", (req, res)=> {
        let newQuestion = req.body;
        questionController.createQuestion({body:newQuestion})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.put("/:_id", (req, res)=> {
        let params = req.params,
            body = req.body;

        questionController.editQuestion({params, body})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.get("/", (req, res)=> {
        let query = req.query;
        questionController.findAllQuestion({query})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.get("/:_id", (req, res)=> {
        let params = req.params;
        questionController.findQuestion({params})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    router.delete("/:_id", (req, res)=> {
        let params = req.params;
        questionController.deleteQuestion({params})
        .then( (resp)=> {
            res.status(200);
            res.send(resp.result)
        })
        .catch( (err)=> {
            res.status(500);
            res.send(err);
        });
    });

    return router;
};
