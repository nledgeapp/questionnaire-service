'use strict';

import mongoose from 'mongoose';

/*********************************
  Add development specific config
  This will override the common/all one
**********************************/
export default {
	mongo:{
        uri:'mongodb://127.0.0.1:27017/edge_app_users',
        options:{}
    }
};
