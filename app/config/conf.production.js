'use strict';

/*********************************
  Add production specific config
  This will override the common/all one
**********************************/
export default {
    mongo:{
        uri: 'mongodb://ds013456.mlab.com:13456/heroku_n0k4305t',
        options:{
            user: 'questionnaire-service-dbuser',
            pass: 'questionnaireservicepw1'
        }
    }
};
