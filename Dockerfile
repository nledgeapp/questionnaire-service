FROM node:6.1.0
RUN mkdir -p /home/app
WORKDIR /home/app
ENV PORT=9000
CMD npm start